import App from './app'
import router from './router'
import store from './store'
import Vue from 'vue'
import { sync } from 'vuex-router-sync'

Vue.config.productionTip = false

sync(store, router)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
