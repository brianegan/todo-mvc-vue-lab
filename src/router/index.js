import Vue from 'vue'
import Router from 'vue-router'
import TodoList from '@/components/todo-list'

Vue.use(Router)

export default new Router({
  linkExactActiveClass: 'selected',
  routes: [
    {
      path: '/',
      name: 'All',
      component: TodoList
    },
    {
      path: '/active',
      name: 'Active',
      component: TodoList
    },
    {
      path: '/completed',
      name: 'Completed',
      component: TodoList
    }
  ]
})
