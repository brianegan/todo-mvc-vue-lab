export default {
  toggleAll ({commit, state, getters}) {
    const complete = !getters.allComplete

    commit('setTodos',
      state.todos.map((todo) => ({...todo, complete}))
    )
  }
}
