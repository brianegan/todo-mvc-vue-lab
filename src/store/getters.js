export default {
  activeTodos: (state) => state.todos.filter((todo) => !todo.complete),
  allComplete: (state) => state.todos.every((todo) => todo.complete),
  completedTodos: (state) => state.todos.filter((todo) => todo.complete),
  hasCompletedTodos: (state, getters) => !!getters.completedTodos.length,
  hasTodos: (state) => !!state.todos.length,
  newTask: (state) => state.newTask,
  todos: (state) => state.todos,
  visibleTodos (state, getters) {
    switch (state.route.path) {
      case '/':
        return getters.todos
      case '/active':
        return getters.activeTodos
      case '/completed':
        return getters.completedTodos
    }
  }
}
