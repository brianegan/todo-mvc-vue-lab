import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import Promise from 'promise-polyfill'
import createStoragePlugin from './storage'
import Vue from 'vue'
import Vuex from 'vuex'

if (!window.Promise) {
  window.Promise = Promise
}

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  state: {
    todos: [],
    newTask: ''
  },
  actions,
  getters,
  mutations,
  plugins: [createStoragePlugin(localStorage)],
  strict: debug
})
