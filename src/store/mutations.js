export default {
  addTodo (state) {
    if (!state.newTask.trim().length) return

    state.todos.push({
      task: state.newTask,
      complete: false,
      id: Date.now() * Math.random(),
      editing: false
    })

    state.newTask = ''
  },
  clearCompleted (state) {
    state.todos = state.todos.filter((todo) => !todo.complete)
  },
  disableEditing (state, todo) {
    todo.editing = false
  },
  enableEditing (state, todo) {
    todo.editing = true
  },
  markTodoComplete (state, todo) {
    todo.complete = !todo.complete
  },
  removeTodo (state, todo) {
    state.todos = state.todos.filter((t) => t.id !== todo.id)
  },
  setTodos (state, todos) {
    state.todos = todos
  },
  updateNewTask (state, event) {
    state.newTask = event.target.value
  },
  updateTodo (state, {todo, event}) {
    const val = event.target.value.trim()
    if (val) {
      todo.task = val
      todo.editing = false
    } else {
      state.todos = state.todos.filter((t) => t.id !== todo.id)
    }
  }
}
