export default storage => store => {
  const storageKey = '__todo_mvc_storage__'
  const initialItems = storage.getItem(storageKey)

  if (initialItems) {
    store.commit('setTodos', JSON.parse(initialItems))
  }

  store.subscribe((mutation, state) => {
    storage.setItem(storageKey, JSON.stringify((state.todos)))
  })
}
