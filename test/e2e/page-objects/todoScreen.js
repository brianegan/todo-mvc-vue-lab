const commands = {
  addTodo (todo) {
    return this.setValue('@newTodoInput', todo)
      .sendKeys('@newTodoInput', this.client.api.Keys.ENTER)
  },
  toggleAll () {
    return this.click('@toggleAllButton')
  },
  clearCompleted () {
    return this.click('@clearCompletedButton')
  },
  deleteTodo () {
    return this.moveToElement('@todoItem', 10, 10, () => {
      this.waitForElementVisible('@todoItemDelete', 500, () => {
        this.click('@todoItemDelete')
      })
    })
  },
  editTodo (todo) {
    const keys = todo.split('')

    if (!keys.length) {
      keys.push(this.client.api.Keys.BACK_SPACE)
    }

    keys.push(this.client.api.Keys.ENTER)

    return (
      this.moveToElement('@todoItemLabel', 200, 50, () => {
        return (
          this.api
            .doubleClick() // Start Editing
            .doubleClick(() => { // Select all text
              return (
                this
                  .waitForElementVisible('@todoItemEditInput', 5000)
                  .sendKeys('@todoItemEditInput', keys)
              )
            })
        )
      })
    )
  },
  refresh () {
    this.client.api.refresh()

    return this
  },
  navigateToAll () {
    return this.click('@allLink')
  },
  navigateToActive () {
    return this.click('@activeLink')
  },
  navigateToCompleted () {
    return this.click('@completedLink')
  }
}

const expectations = {
  expectRoute (path) {
    return this.assert.urlEquals(`${this.api.globals.devServerURL}/#${path}`)
  },
  expectTitle () {
    return this.expect.element('@title')
  },
  expectList () {
    return this.expect.element('@todoList')
  },
  expectTodo () {
    return this.expect.element('@todoItem')
  },
  expectTodoPresent () {
    return this.expect.element('@todoItem').to.be.present
  },
  expectTodoToggle () {
    return this.expect.element('@todoItemToggle')
  },
  expectTodoLabel () {
    return this.expect.element('@todoItemLabel')
  },
  expectTodoDelete () {
    return this.expect.element('@todoItemDelete')
  },
  expectTodoEditInput () {
    return this.expect.element('@todoItemEditInput')
  },
  expectFooter () {
    return this.expect.element('@footer')
  },
  expectToggleAllCheckbox () {
    return this.expect.element('@toggleAllCheckbox')
  },
  expectClearCompleted () {
    return this.expect.element('@clearCompletedButton')
  },
  expectTodoCount () {
    return this.expect.element('@todoCount')
  }
}

module.exports = {
  commands: [commands, expectations],
  url () {
    return this.api.globals.devServerURL
  },
  elements: {
    app: {
      selector: '.todoapp'
    },
    title: {
      selector: 'h1'
    },
    newTodoInput: {
      selector: '.new-todo'
    },
    main: {
      selector: '.main'
    },
    toggleAllCheckbox: {
      selector: '.toggle-all'
    },
    toggleAllButton: {
      selector: 'label[for=toggle-all]'
    },
    todoList: {
      selector: '.todo-list'
    },
    todoItem: {
      selector: '.todo-list li:first-child'
    },
    todoItemToggle: {
      selector: '.todo-list li:first-child .toggle'
    },
    todoItemLabel: {
      selector: '.todo-list li:first-child label'
    },
    todoItemEditInput: {
      selector: '.edit'
    },
    todoItemDelete: {
      selector: '.destroy'
    },
    todoCount: {
      selector: '.todo-count'
    },
    footer: {
      selector: '.footer'
    },
    clearCompletedButton: {
      selector: '.clear-completed'
    },
    allLink: {
      selector: '.filters :nth-child(1) a'
    },
    activeLink: {
      selector: '.filters :nth-child(2) a'
    },
    completedLink: {
      selector: '.filters :nth-child(3) a'
    }
  }
}
