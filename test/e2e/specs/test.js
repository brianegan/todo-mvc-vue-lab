/* eslint-disable no-unused-expressions */
const todo = 'test'
let app

module.exports = {
  before (browser) {
    browser.page.todoScreen().navigate()
  },
  beforeEach (browser) {
    app = browser.page.todoScreen()
  },
  after (browser) {
    browser.end()
  },
  'the page should have a title' () {
    app.expectTitle().to.be.present
  },
  'no list or footer should appear at first' () {
    app.expectList().to.not.be.present
    app.expectFooter().to.not.be.present
  },
  'you should be able to add a todo that is incomplete' () {
    app.addTodo(todo).expectTodoPresent()
    app.expectTodo().text.to.contain(todo)
    app.expectTodoToggle().to.not.be.selected
    app.expectTodoEditInput().to.not.be.present
  },
  'the app should save the todos upon refresh' () {
    app.refresh().expectTodoPresent()
  },
  'once the todo exists, the footer should appear' () {
    app.expectFooter().to.be.present
    app.expectTodoCount().text.to.equal('1 item left')
  },
  'once a todo exists, the toggle all button should be visible and unchecked' () {
    app.expectToggleAllCheckbox().to.not.be.selected
  },
  'if there are no completed todos, clear completed button should not be visible' () {
    app.expectClearCompleted().to.not.be.present
  },
  'click toggle all button should update the todo and clear completed button' () {
    app.toggleAll()

    app.expectClearCompleted().to.be.present
    app.expectTodoToggle().to.be.selected
    app.expectTodoCount().text.to.equal('0 items left')
  },
  'clicking clear completed should delete the todos' () {
    app.expectTodo().to.be.present

    app.clearCompleted()

    app.expectTodo().to.not.be.present
    app.addTodo(todo)
  },
  'clicking the delete todo button should remove the todo' () {
    app.expectTodo().to.be.present
    app.expectTodoDelete().to.be.present

    app.deleteTodo()

    app.expectTodo().to.not.be.present
    app.addTodo(todo)
  },
  'double tapping the todo should allow you to edit the todo' () {
    const newTodo = 'new'

    app.expectTodoLabel().to.be.present
    app.editTodo(newTodo)

    app.expectTodoLabel().text.to.contain(newTodo)
  },
  'removing all text from the todo will delete the todo' () {
    const newTodo = ''

    app.expectTodoLabel().to.be.present
    app.editTodo(newTodo)

    app.expectTodo().to.not.be.present

    app.addTodo(todo)
  },
  'clicking on active link navigates to /active and shows only completed todos' (browser) {
    app.navigateToActive()

    app.expectRoute(`/active`)
    app.expectTodo().to.be.present

    app.toggleAll()

    app.expectTodo().to.not.be.present
  },
  'clicking on completed link navigates to /completed and shows only completed todos' (browser) {
    app.navigateToCompleted()

    app.expectRoute(`/completed`)
    app.expectTodo().to.be.present

    app.toggleAll()

    app.expectTodo().to.not.be.present
  },
  'clicking on all link navigates back to / and shows all todos' (browser) {
    app.navigateToAll()

    app.expectRoute(`/`)
    app.expectTodo().to.be.present

    app.toggleAll()

    app.expectTodo().to.be.present
  }
}
