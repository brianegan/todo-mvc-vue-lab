import actions from '../../../src/store/actions'

describe('Todo Actions', () => {
  it('should mark all as incomplete if all todos complete', () => {
    let captor = []
    const state = {
      todos: [
        {
          complete: true
        },
        {
          complete: true
        }
      ],
      newTask: 'Task'
    }
    const commit = function (mutation, todos) {
      expect(mutation).to.equal('setTodos')
      captor = todos
    }

    actions.toggleAll({commit, state, getters: {allComplete: true}})

    expect(!!captor.length).to.equal(true)
    expect(captor.every((todo) => !todo.complete)).to.equal(true)
  })

  it('should mark all as complete if some not complete', () => {
    let captor = []
    const state = {
      todos: [
        {
          complete: false
        },
        {
          complete: true
        }
      ],
      newTask: 'Task'
    }
    const commit = function (mutation, todos) {
      expect(mutation).to.equal('setTodos')
      captor = todos
    }

    actions.toggleAll({commit, state, getters: {allComplete: false}})

    expect(!!captor.length).to.equal(true)
    expect(captor.every((todo) => todo.complete)).to.equal(true)
  })
})
