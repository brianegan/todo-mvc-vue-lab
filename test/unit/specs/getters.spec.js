import getters from '../../../src/store/getters'

describe('Todo Getters', () => {
  it('should get the todos', () => {
    const todos = [{task: 'hi'}]
    const state = {
      todos,
      newTask: 'Task'
    }

    expect(getters.todos(state)).to.equal(todos)
  })

  it('should get the newTask', () => {
    const newTask = 'Task'
    const state = {
      newTask
    }

    expect(getters.newTask(state)).to.equal(newTask)
  })

  it('should get the active todos', () => {
    const state = {
      todos: [
        {
          complete: true
        },
        {
          complete: false
        },
        {
          complete: false
        }
      ]
    }

    expect(getters.activeTodos(state).length).to.equal(2)
  })

  it('should get the complete todos', () => {
    const state = {
      todos: [
        {
          complete: true
        },
        {
          complete: false
        },
        {
          complete: false
        }
      ]
    }

    expect(getters.completedTodos(state).length).to.equal(1)
  })

  it('should not be all complete if some todos are incomplete', () => {
    const state = {
      todos: [
        {
          complete: true
        },
        {
          complete: false
        },
        {
          complete: false
        }
      ]
    }

    expect(getters.allComplete(state)).to.equal(false)
  })

  it('should be all complete if all todos are complete', () => {
    const state = {
      todos: [
        {
          complete: true
        },
        {
          complete: true
        },
        {
          complete: true
        }
      ]
    }

    expect(getters.allComplete(state)).to.equal(true)
  })

  it('should report having complete todos', () => {
    const state = {
      todos: [
        {
          complete: true
        }
      ]
    }

    expect(getters.hasCompletedTodos(state, {completedTodos: getters.completedTodos(state)})).to.equal(true)
  })

  it('should report having no complete todos', () => {
    const state = {
      todos: [
        {
          complete: false
        }
      ]
    }

    expect(getters.hasCompletedTodos(state, {completedTodos: getters.completedTodos(state)})).to.equal(false)
  })

  it('should report having todos if any todos exist', () => {
    const state = {
      todos: [
        {
          complete: true
        }
      ]
    }

    expect(getters.hasTodos(state)).to.equal(true)
  })

  it('should report having no todos if none exist', () => {
    const state = {
      todos: []
    }

    expect(getters.hasTodos(state)).to.equal(false)
  })

  it('should show all todos at root path', () => {
    const state = {
      route: {
        path: '/'
      },
      todos: [
        {
          complete: true
        },
        {
          complete: false
        },
        {
          complete: false
        }
      ]
    }

    expect(getters.visibleTodos(state, {todos: getters.todos(state)}).length).to.equal(3)
  })

  it('should show completed todos at /completed path', () => {
    const state = {
      route: {
        path: '/completed'
      },
      todos: [
        {
          complete: true
        },
        {
          complete: false
        },
        {
          complete: false
        }
      ]
    }

    expect(getters.visibleTodos(state, {completedTodos: getters.completedTodos(state)}).length).to.equal(1)
  })

  it('should show active todos at /active path', () => {
    const state = {
      route: {
        path: '/active'
      },
      todos: [
        {
          complete: true
        },
        {
          complete: false
        },
        {
          complete: false
        }
      ]
    }

    expect(getters.visibleTodos(state, {activeTodos: getters.activeTodos(state)}).length).to.equal(2)
  })
})
