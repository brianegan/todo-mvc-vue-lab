import mutations from '../../../src/store/mutations'

describe('Todo Mutations', () => {
  it('should add a todo if newTask is not empty', () => {
    const state = {
      todos: [],
      newTask: 'Task'
    }

    mutations.addTodo(state)

    expect(state.todos.length).to.equal(1)
  })

  it('should not add a todo if newTask is empty', () => {
    const state = {
      todos: [],
      newTask: ''
    }

    mutations.addTodo(state)

    expect(state.todos.length).to.equal(0)
  })

  it('should clear completed', () => {
    const state = {
      todos: [{complete: true}]
    }

    expect(state.todos.length).to.equal(1)

    mutations.clearCompleted(state)

    expect(state.todos.length).to.equal(0)
  })

  it('should disable editing on a todo', () => {
    const todo = {
      editing: true
    }

    mutations.disableEditing(null, todo)

    expect(todo.editing).to.equal(false)
  })

  it('should enable editing on a todo', () => {
    const todo = {
      editing: false
    }

    mutations.enableEditing(null, todo)

    expect(todo.editing).to.equal(true)
  })

  it('should mark a todo complete', () => {
    const todo = {
      complete: false
    }

    mutations.markTodoComplete(null, todo)

    expect(todo.complete).to.equal(true)
  })

  it('should remove a todo from the list', () => {
    const first = {
      task: 'first',
      id: 1
    }
    const state = {
      todos: [
        first,
        {
          task: 'second',
          id: 2
        }
      ]
    }

    expect(state.todos.length).to.equal(2)

    mutations.removeTodo(state, first)

    expect(state.todos.length).to.equal(1)
  })

  it('should remove a todo from the list', () => {
    const state = {
      todos: [
        {
          task: 'first',
          id: 1
        }
      ]
    }

    expect(state.todos.length).to.equal(1)

    mutations.setTodos(state, [])

    expect(state.todos.length).to.equal(0)
  })

  it('should update the new task', () => {
    const value = 'newTask'
    const state = {
      todos: [],
      newTask: ''
    }

    mutations.updateNewTask(state, {target: {value}})

    expect(state.newTask).to.equal(value)
  })

  it('should update the todo if the new value is not empty', () => {
    const value = 'newTask'
    const todo = {
      task: 'hi',
      editing: true
    }
    const state = {
      todos: [todo],
      newTask: ''
    }

    mutations.updateTodo(state, {
      todo,
      event: {target: {value}}
    })

    expect(todo.task).to.equal(value)
    expect(todo.editing).to.equal(false)
  })

  it('should delete the todo if the new value is empty', () => {
    const value = ''
    const todo = {
      task: 'hi',
      editing: true
    }
    const state = {
      todos: [todo],
      newTask: ''
    }

    mutations.updateTodo(state, {
      todo,
      event: {target: {value}}
    })

    expect(state.todos.length).to.equal(0)
  })
})
