import Vuex from 'vuex'
import createStoragePlugin from '../../../src/store/storage'

describe('Todo Storage', () => {
  it('should save the new todos after the commit', () => {
    const storage = {
      cache: {},

      getItem (key) {
        return this.cache[key]
      },

      setItem (key, val) {
        this.cache[key] = val
      }
    }
    const initialTodos = [{
      task: 'hi',
      complete: false,
      editing: false
    }]
    const store = new Vuex.Store({
      state: {
        todos: initialTodos
      },
      mutations: {
        setTodos (state, todos) {
          state.todos = todos
        }
      }
    })
    const plugin = createStoragePlugin(storage)

    plugin(store)

    expect(store.state.todos).to.equal(initialTodos)

    const newTodos = [{
      task: 'no'
    }]
    store.commit('setTodos', newTodos)

    expect(storage.getItem('__todo_mvc_storage__')).to.equal(JSON.stringify(newTodos))
  })

  it('should load the todos that exist in storage', () => {
    const storedTodos = [{task: 'new'}]
    const storage = {
      getItem () {
        return JSON.stringify(storedTodos)
      }
    }
    const initialTodos = [{
      task: 'hi',
      complete: false,
      editing: false
    }]
    const store = new Vuex.Store({
      state: {
        todos: initialTodos
      },
      mutations: {
        setTodos (state, todos) {
          state.todos = todos
        }
      }
    })
    const plugin = createStoragePlugin(storage)

    plugin(store)

    expect(store.state.todos).to.deep.equal(storedTodos)
  })
})
